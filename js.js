$(function(){
  var slickOpts = {
    dots: true,
    infinite: true,
    arrow: false,
    slidesToShow: 2,
    adaptiveHeight: true
};

$('.people_slider').slick(slickOpts);
});